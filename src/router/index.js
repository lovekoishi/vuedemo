import Vue from 'vue'
import VueRouter from 'vue-router'
import UserView from "@/views/UserView";
import OrderView from "@/views/OrderView";
import UserManagement from "@/views/UserManagement";
import RoleManagement from "@/views/RoleManagement";
import Index from "@/views/Index"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "权限管理系统",
    component: Index,
    children: [
      {
        path: "/UserView",
        name: "查看用户",
        component: UserView
      },
      {
        path: "/OrderView",
        name: "查看订单",
        component: OrderView
      },
      {
        path: "/UserManagement",
        name: "用户配置",
        component: UserManagement
      },
      {
        path: "/RoleManagement",
        name: "角色配置",
        component: RoleManagement
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
